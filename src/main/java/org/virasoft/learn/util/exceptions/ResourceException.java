package org.virasoft.learn.util.exceptions;

/**
 * @author vjayad1
 */
public class ResourceException extends Exception {

    public ResourceException(String message) {
        super(message);
    }
}
