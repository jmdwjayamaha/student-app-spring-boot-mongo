package org.virasoft.learn.util.exceptions;

/**
 * @author vjayad1
 */
public class ResourceNotFoundException extends ResourceException {

    public ResourceNotFoundException(final String resource) {
        super(resource + " not found");
    }
}
