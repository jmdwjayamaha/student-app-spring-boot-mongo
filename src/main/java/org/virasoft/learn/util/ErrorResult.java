package org.virasoft.learn.util;

/**
 * @author vjayad1
 */
public class ErrorResult {

    private String error;

    private String message;

    public ErrorResult(String message) {
        this.error = "Cannot perform action";
        this.message = message;
    }

    public ErrorResult(final String error, final String message) {
        this.error = error;
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
