package org.virasoft.learn.services;

import org.virasoft.learn.model.Student;
import org.virasoft.learn.util.exceptions.ResourceNotFoundException;

/**
 * @author vjayad1
 */
public interface StudentService {

    Student save(Student student);

    Student update(String id, Student student) throws ResourceNotFoundException;

    Student getById(String id) throws ResourceNotFoundException;

    void delete(String id) throws ResourceNotFoundException;

    Iterable<Student> listStudents(String name, String email);
}
