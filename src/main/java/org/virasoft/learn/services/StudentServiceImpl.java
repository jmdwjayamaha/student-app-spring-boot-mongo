package org.virasoft.learn.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.virasoft.learn.model.Student;
import org.virasoft.learn.repository.StudentRepository;
import org.virasoft.learn.util.StringUtils;
import org.virasoft.learn.util.exceptions.ResourceNotFoundException;

import java.util.HashSet;
import java.util.Set;

/**
 * @author vjayad1
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Student save(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student update(String id, Student student) throws ResourceNotFoundException {

        Student selectedStudent = studentRepository.findOne(id);

        if (selectedStudent == null) {
            throw new ResourceNotFoundException("Student");
        }

        selectedStudent.setEmail(student.getEmail());
        selectedStudent.setName(student.getName());
        return studentRepository.save(selectedStudent);
    }

    @Override
    public Student getById(String id) throws ResourceNotFoundException {

        Student selectedStudent = studentRepository.findOne(id);

        if (selectedStudent == null) {
            throw new ResourceNotFoundException("Student");
        }

        return selectedStudent;
    }

    @Override
    public void delete(String id) throws ResourceNotFoundException {

        Student selectedStudent = studentRepository.findOne(id);

        if (selectedStudent == null) {
            throw new ResourceNotFoundException("Student");
        }

        studentRepository.delete(selectedStudent);
    }

    @Override
    public Iterable<Student> listStudents(final String name, final String email) {

        Iterable<Student> studentList;

        if (StringUtils.isNullOrEmpty(name) && StringUtils.isNullOrEmpty(email)) {
            studentList = studentRepository.findAll();
        } else {
            if (StringUtils.isNullOrEmpty(email)) {
                studentList = studentRepository.findByNameLike(name);
            } else if (StringUtils.isNullOrEmpty(name)) {
                studentList = studentRepository.findByEmailLike(email);
            } else {
                Iterable<Student> studentListByName = studentRepository.findByNameLike(name);
                Iterable<Student> studentListByEmail = studentRepository.findByEmailLike(email);

                Set<Student> nonDupStuList = new HashSet<>();

                studentListByName.forEach(student -> nonDupStuList.add(student));

                studentListByEmail.forEach(student -> nonDupStuList.add(student));

                studentList = nonDupStuList;
            }
        }

        return studentList;
    }
}
