package org.virasoft.learn.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.virasoft.learn.util.ErrorResult;
import org.virasoft.learn.util.exceptions.ResourceException;

/**
 * @author vjayad1
 */
public class BaseController {

    @ExceptionHandler(ResourceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResult resourceExceptionHandler(ResourceException exc) {

        return new ErrorResult(exc.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResult exceptionHandler(Exception exc) {

        System.out.println(exc.getClass() + " : " + exc.getMessage());
        return new ErrorResult("Internal Server Error");
    }
}
