package org.virasoft.learn.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.virasoft.learn.model.Student;
import org.virasoft.learn.services.StudentService;
import org.virasoft.learn.util.exceptions.ResourceNotFoundException;

/**
 * @author vjayad1
 */
@RestController
@RequestMapping("/student")
public class StudentController extends BaseController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(method = RequestMethod.POST)
    public Student save(@RequestBody Student student) {

        return studentService.save(student);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Student> listStudents(String name, String email) {

        return studentService.listStudents(name, email);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Student getById(@PathVariable("id") String id) throws ResourceNotFoundException {

        return studentService.getById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Student updateStudent(@PathVariable("id") String id, @RequestBody Student student) throws
            ResourceNotFoundException {

        return studentService.update(id, student);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteStudent(@PathVariable("id") String id) throws ResourceNotFoundException {

        studentService.delete(id);
    }
}
