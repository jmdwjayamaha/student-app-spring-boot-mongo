package org.virasoft.learn.repository;

import org.springframework.data.repository.CrudRepository;
import org.virasoft.learn.model.Student;

/**
 * @author vjayad1
 */
public interface StudentRepository extends CrudRepository<Student, String> {

    Iterable<Student> findByNameLike(String name);

    Iterable<Student> findByEmailLike(String email);
}
